<?php

namespace Apprendible\FirstPackage\Tests;

// use Orchestra\Testbench\TestCase;
use Orchestra\Testbench\PHPUnit\TestCase;
use Apprendible\FirstPackage\Facades\FirstPackage;
use Apprendible\FirstPackage\FirstPackageServiceProvider;

class HelloTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            FirstPackageServiceProvider::class
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            "FirstPackage" => FirstPackage::class
            ];
    }

    /** @test */
    function it_returns_the_message()
    {
        $this->assertEquals(
            "Halo Kevin, this is the first package", 
            FirstPackage::hello()
        );
    }
}
