<?php

namespace Apprendible\FirstPackage;

use Illuminate\Support\ServiceProvider;

class FirstPackageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }


    public function register()
    {
        $this->app->bind('first-package', function () {
            return new Hello;
        });
    }
}