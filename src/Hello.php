<?php

namespace Apprendible\FirstPackage;

class Hello
{
    protected $name;

    public function __construct($name = "Kistler")
    {
        $this->name = $name;
    }

    public function hello()
    {
        return "Halo $this->name, this is the first package";
    }
}